package br.com.itau.ExercicioCartoes.services;

import br.com.itau.ExercicioCartoes.DTOs.PagamentoEntradaDTO;
import br.com.itau.ExercicioCartoes.DTOs.PagamentoSaidaDTO;
import br.com.itau.ExercicioCartoes.models.Cartao;
import br.com.itau.ExercicioCartoes.models.Pagamento;
import br.com.itau.ExercicioCartoes.repositories.PagamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PagamentoService
{
    @Autowired
    private PagamentoRepository pagamentoRepository;

    @Autowired
    private CartaoService cartaoService;

    public PagamentoSaidaDTO criar(PagamentoEntradaDTO pagamentoDTO)
    {
        Cartao cartao = cartaoService.buscarPorId(pagamentoDTO.getCartao_id());

        if(cartao != null)
        {
            Pagamento pagamento = new Pagamento();
            pagamento.setCartao(cartao);
            pagamento.setDescricao(pagamentoDTO.getDescricao());
            pagamento.setValor(pagamentoDTO.getValor());

            pagamento = pagamentoRepository.save(pagamento);

            return new PagamentoSaidaDTO(pagamento);
        }
        else
        {
            throw new RuntimeException("Cartão não encontrado");
        }
    }

    public List<PagamentoSaidaDTO> buscarPorId_cartao(int id)
    {
        Cartao cartao = cartaoService.buscarPorId(id);

        if(cartao != null)
        {
            List<Pagamento> lista = pagamentoRepository.findByCartao(cartao);

            List<PagamentoSaidaDTO> extrato = new ArrayList<>();

            for(Pagamento pagamento : lista)
            {
                extrato.add(new PagamentoSaidaDTO(pagamento));
            }

            return extrato;
        }
        else
        {
            throw new RuntimeException("Cartão não encontrado");
        }
    }
}
