package br.com.itau.ExercicioCartoes.services;

import br.com.itau.ExercicioCartoes.DTOs.CartaoEntradaDTO;
import br.com.itau.ExercicioCartoes.DTOs.CartaoSaidaDTO;
import br.com.itau.ExercicioCartoes.DTOs.CartaoSemStatusDTO;
import br.com.itau.ExercicioCartoes.DTOs.CartaoStatusDTO;
import br.com.itau.ExercicioCartoes.models.Cartao;
import br.com.itau.ExercicioCartoes.models.Cliente;
import br.com.itau.ExercicioCartoes.repositories.CartaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CartaoService
{
    @Autowired
    private CartaoRepository cartaoRepository;

    @Autowired
    private ClienteService clienteService;

    public CartaoSaidaDTO criar(CartaoEntradaDTO cartaoDTO)
    {
        Cliente cliente = clienteService.buscarPorId(cartaoDTO.getClienteId());

        if(cliente != null)
        {
            Cartao cartaoDb = cartaoRepository.findByNumero(cartaoDTO.getNumero());

            if(cartaoDb != null)
            {
                throw new RuntimeException("Número de cartão já utilizado");
            }
            else
            {
                Cartao cartao = new Cartao();
                cartao.setCliente(cliente);
                cartao.setNumero(cartaoDTO.getNumero());
                cartao.setAtivo(false);

                cartao = cartaoRepository.save(cartao);

                return new CartaoSaidaDTO(cartao);
            }
        }
        else
        {
            throw new RuntimeException("Cliente não encontrado");
        }
    }

    public Cartao buscarPorId(Integer id)
    {
        Optional<Cartao> cartaoOptional = cartaoRepository.findById(id);

        if(cartaoOptional.isPresent())
        {
            Cartao cartao = cartaoOptional.get();
            return cartao;
        }
        else
        {
            throw new RuntimeException("O cartão não foi encontrado");
        }
    }

    public CartaoSaidaDTO alterarStatusAtivo(String numero, CartaoStatusDTO cartaoStatusDTO)
    {
        Cartao cartao = cartaoRepository.findByNumero(numero);

        if(cartao != null)
        {
            cartao.setAtivo(cartaoStatusDTO.getAtivo());

            cartao = cartaoRepository.save(cartao);

            return new CartaoSaidaDTO(cartao);
        }
        else
        {
            throw new RuntimeException("Número de cartão não existe");
        }
    }

    public CartaoSemStatusDTO buscarPorNumero(String numero)
    {
        Cartao cartao = cartaoRepository.findByNumero(numero);

        if(cartao != null)
        {
            return new CartaoSemStatusDTO(cartao);
        }
        else
        {
            throw new RuntimeException("Número de cartão não existe");
        }
    }
}
