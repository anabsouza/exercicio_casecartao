package br.com.itau.ExercicioCartoes.repositories;

import br.com.itau.ExercicioCartoes.models.Cartao;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CartaoRepository extends JpaRepository<Cartao,Integer >
{
    Cartao findByNumero(String numero);
}
