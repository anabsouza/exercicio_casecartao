package br.com.itau.ExercicioCartoes.repositories;

import br.com.itau.ExercicioCartoes.models.Cliente;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClienteRepository extends JpaRepository<Cliente,Integer > {
}
