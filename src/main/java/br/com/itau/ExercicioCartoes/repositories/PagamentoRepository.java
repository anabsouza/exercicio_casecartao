package br.com.itau.ExercicioCartoes.repositories;

import br.com.itau.ExercicioCartoes.models.Cartao;
import br.com.itau.ExercicioCartoes.models.Pagamento;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PagamentoRepository extends JpaRepository<Pagamento,Integer >
{
    List<Pagamento> findByCartao(Cartao cartao);
}
