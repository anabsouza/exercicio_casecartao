package br.com.itau.ExercicioCartoes.DTOs;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class CartaoEntradaDTO
{
    @NotNull(message =  "Número do cartao não pode ser nulo")
    @NotBlank(message = "Número do cartao não pode ser vazio")
    private String numero;

    @NotNull(message =  "ClienteId não pode ser nulo")
    private Integer clienteId;

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public Integer getClienteId() {
        return clienteId;
    }

    public void setClienteId(Integer clienteId) {
        this.clienteId = clienteId;
    }

    public CartaoEntradaDTO() {
    }
}
