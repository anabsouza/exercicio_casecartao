package br.com.itau.ExercicioCartoes.DTOs;

import br.com.itau.ExercicioCartoes.models.Cartao;
import br.com.itau.ExercicioCartoes.models.Pagamento;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class PagamentoSaidaDTO
{
    private Integer id;

    private Integer cartao_id;

    private String descricao;

    private Double valor;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCartao_id() {
        return cartao_id;
    }

    public void setCartao_id(Integer cartao_id) {
        this.cartao_id = cartao_id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public PagamentoSaidaDTO() {
    }

    public PagamentoSaidaDTO(Pagamento pagamento)
    {
        this.setCartao_id(pagamento.getCartao().getId());
        this.setDescricao(pagamento.getDescricao());
        this.setId(pagamento.getId());
        this.setValor(pagamento.getValor());
    }
}
