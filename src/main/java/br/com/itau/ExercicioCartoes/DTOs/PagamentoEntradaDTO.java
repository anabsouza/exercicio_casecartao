package br.com.itau.ExercicioCartoes.DTOs;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class PagamentoEntradaDTO
{
    @NotNull(message =  "CartaoId não pode ser nulo")
    private Integer cartao_id;

    @NotNull(message =  "Descrição não pode ser nula")
    @NotBlank(message = "Descrição não pode ser vazia")
    private String descricao;

    @NotNull(message =  "Valor não pode ser nulo")
    @DecimalMin(value = "0", message = "Valor deve ser maior ou igual a zero")
    @Digits(integer = 6, fraction = 2, message = "Valor fora do padrão")
    private Double valor;

    public Integer getCartao_id() {
        return cartao_id;
    }

    public void setCartao_id(Integer cartao_id) {
        this.cartao_id = cartao_id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public PagamentoEntradaDTO() {
    }
}
