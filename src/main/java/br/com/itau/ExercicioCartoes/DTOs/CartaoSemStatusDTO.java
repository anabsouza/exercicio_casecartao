package br.com.itau.ExercicioCartoes.DTOs;

import br.com.itau.ExercicioCartoes.models.Cartao;

public class CartaoSemStatusDTO
{
    private Integer id;

    private String numero;

    private Integer clienteId;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public Integer getClienteId() {
        return clienteId;
    }

    public void setClienteId(Integer clienteId) {
        this.clienteId = clienteId;
    }

    public CartaoSemStatusDTO() {
    }

    public CartaoSemStatusDTO(Cartao cartao)
    {
        this.setClienteId(cartao.getCliente().getId());
        this.setId(cartao.getId());
        this.setNumero(cartao.getNumero());
    }
}
