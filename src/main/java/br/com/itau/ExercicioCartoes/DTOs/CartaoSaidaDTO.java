package br.com.itau.ExercicioCartoes.DTOs;

import br.com.itau.ExercicioCartoes.models.Cartao;
import br.com.itau.ExercicioCartoes.models.Cliente;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class CartaoSaidaDTO
{
    private Integer id;

    private String numero;

    private Integer clienteId;

    private Boolean ativo;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public Integer getClienteId() {
        return clienteId;
    }

    public void setClienteId(Integer clienteId) {
        this.clienteId = clienteId;
    }

    public Boolean getAtivo() {
        return ativo;
    }

    public void setAtivo(Boolean ativo) {
        this.ativo = ativo;
    }

    public CartaoSaidaDTO() {
    }

    public CartaoSaidaDTO(Cartao cartao)
    {
        this.setAtivo(cartao.getAtivo());
        this.setClienteId(cartao.getCliente().getId());
        this.setId(cartao.getId());
        this.setNumero(cartao.getNumero());
    }
}
