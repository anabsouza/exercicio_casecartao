package br.com.itau.ExercicioCartoes.controllers;

import br.com.itau.ExercicioCartoes.DTOs.PagamentoEntradaDTO;
import br.com.itau.ExercicioCartoes.DTOs.PagamentoSaidaDTO;
import br.com.itau.ExercicioCartoes.models.Cliente;
import br.com.itau.ExercicioCartoes.services.PagamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
public class PagamentoController
{
    @Autowired
    PagamentoService pagamentoService;

    @PostMapping("/pagamento")
    @ResponseStatus(HttpStatus.CREATED)
    public PagamentoSaidaDTO criar(@RequestBody @Valid PagamentoEntradaDTO pagamento)
    {
        return  pagamentoService.criar(pagamento);
    }

    @GetMapping("/pagamentos/{id_cartao}")
    public List<PagamentoSaidaDTO> buscarPorId_cartao(@PathVariable(name = "id_cartao") int id_cartao)
    {
        try
        {
            List<PagamentoSaidaDTO> pagamentos = pagamentoService.buscarPorId_cartao(id_cartao);
            return pagamentos;
        }
        catch (RuntimeException e)
        {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }
}
