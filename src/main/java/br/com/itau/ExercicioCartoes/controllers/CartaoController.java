package br.com.itau.ExercicioCartoes.controllers;

import br.com.itau.ExercicioCartoes.DTOs.CartaoEntradaDTO;
import br.com.itau.ExercicioCartoes.DTOs.CartaoSaidaDTO;
import br.com.itau.ExercicioCartoes.DTOs.CartaoSemStatusDTO;
import br.com.itau.ExercicioCartoes.DTOs.CartaoStatusDTO;
import br.com.itau.ExercicioCartoes.services.CartaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/cartao")
public class CartaoController
{
    @Autowired
    CartaoService cartaoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public CartaoSaidaDTO criar(@RequestBody @Valid CartaoEntradaDTO cartao)
    {
        return  cartaoService.criar(cartao);
    }

    @PatchMapping("/{numero}")
    public CartaoSaidaDTO alterarStatusAtivo(@PathVariable(name = "numero") String numero, @RequestBody @Valid CartaoStatusDTO cartaoStatusDTO)
    {
        try
        {
            return cartaoService.alterarStatusAtivo(numero, cartaoStatusDTO);
        }
        catch (RuntimeException e)
        {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @GetMapping("/{numero}")
    public CartaoSemStatusDTO buscarPorNumero(@PathVariable(name = "numero") String numero)
    {
        try
        {
            return cartaoService.buscarPorNumero(numero);
        }
        catch (RuntimeException e)
        {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }
}
