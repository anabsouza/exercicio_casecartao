package br.com.itau.ExercicioCartoes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExercCaseCrtApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExercCaseCrtApplication.class, args);
	}

}
