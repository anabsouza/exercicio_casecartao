package br.com.itau.ExercicioCartoes.services;

import br.com.itau.ExercicioCartoes.DTOs.CartaoEntradaDTO;
import br.com.itau.ExercicioCartoes.DTOs.CartaoSaidaDTO;
import br.com.itau.ExercicioCartoes.DTOs.CartaoSemStatusDTO;
import br.com.itau.ExercicioCartoes.DTOs.CartaoStatusDTO;
import br.com.itau.ExercicioCartoes.models.Cartao;
import br.com.itau.ExercicioCartoes.models.Cliente;
import br.com.itau.ExercicioCartoes.repositories.CartaoRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDate;
import java.util.Optional;

@SpringBootTest
public class CartaoServiceTeste
{
    @MockBean
    private CartaoRepository cartaoRepository;

    @MockBean
    private ClienteService clienteService;

    @Autowired
    private CartaoService cartaoService;

    @Test
    public void testarCriar()
    {
        //Preparação
        Cliente cliente = new Cliente();
        cliente.setId(1);
        cliente.setName("Jose");

        Cartao cartao = new Cartao();
        cartao.setCliente(cliente);
        cartao.setNumero("123");
        cartao.setAtivo(true);

        CartaoEntradaDTO cartaoDTO = new CartaoEntradaDTO();
        cartaoDTO.setNumero("123");
        cartaoDTO.setClienteId(1);

        Mockito.when(clienteService.buscarPorId(Mockito.anyInt())).thenReturn(cliente);
        Mockito.when(cartaoRepository.findByNumero(Mockito.anyString())).thenReturn(null);
        Mockito.when(cartaoRepository.save(Mockito.any(Cartao.class))).thenReturn(cartao);

        //Execução
        CartaoSaidaDTO cartaoResultado = cartaoService.criar(cartaoDTO);

        //Verificação
        Assertions.assertEquals(cartao.getNumero(), cartaoResultado.getNumero());
        Assertions.assertEquals(cartao.getCliente().getId(), cartaoResultado.getClienteId());
    }

    @Test
    public void testarCriarComNumeroExistente()
    {
        //Preparação
        Cliente cliente = new Cliente();
        cliente.setId(1);
        cliente.setName("Jose");

        Cartao cartao = new Cartao();
        cartao.setCliente(cliente);
        cartao.setNumero("123");
        cartao.setAtivo(true);

        CartaoEntradaDTO cartaoDTO = new CartaoEntradaDTO();
        cartaoDTO.setNumero("123");
        cartaoDTO.setClienteId(1);

        Mockito.when(clienteService.buscarPorId(Mockito.anyInt())).thenReturn(cliente);
        Mockito.when(cartaoRepository.findByNumero(Mockito.anyString())).thenReturn(cartao);

        //Execução + Verificação
        Assertions.assertThrows(RuntimeException.class, () -> {cartaoService.criar(cartaoDTO);});
    }

    @Test
    public void testarCriarComClienteInexistente()
    {
        //Preparação
        CartaoEntradaDTO cartaoDTO = new CartaoEntradaDTO();
        cartaoDTO.setNumero("123");
        cartaoDTO.setClienteId(1);

        Mockito.when(clienteService.buscarPorId(Mockito.anyInt())).thenReturn(null);

        //Execução + Verificação
        Assertions.assertThrows(RuntimeException.class, () -> {cartaoService.criar(cartaoDTO);});
    }

    @Test
    public void testarBuscarPorIdEncontrandoCartao()
    {
        //Preparação
        Cliente cliente = new Cliente();
        cliente.setId(1);
        cliente.setName("Jose");

        Cartao cartao = new Cartao();
        cartao.setCliente(cliente);
        cartao.setNumero("123");
        cartao.setAtivo(true);

        CartaoEntradaDTO cartaoDTO = new CartaoEntradaDTO();
        cartaoDTO.setNumero("123");
        cartaoDTO.setClienteId(1);

        Optional<Cartao> cartaoOptional = Optional.of(cartao);
        Mockito.when(cartaoRepository.findById(Mockito.anyInt())).thenReturn(cartaoOptional);

        //Execução
        Cartao cartaoCriado = cartaoService.buscarPorId(1);

        //Verificação
        Assertions.assertEquals(cartaoOptional.get(), cartaoCriado);
    }

    @Test
    public void testarBuscarPorIdProdutoNaoEncontrado()
    {
        //Preparação
        Optional<Cartao> produtoOptional = Optional.empty();
        Mockito.when(cartaoRepository.findById(Mockito.anyInt())).thenReturn(produtoOptional);

        //Execução + Verificação
        Assertions.assertThrows(RuntimeException.class, () -> {cartaoService.buscarPorId(1);});
    }

    @Test
    public void testarAlterarStatusAtivo()
    {
        //Preparação
        Cliente cliente = new Cliente();
        cliente.setId(1);
        cliente.setName("Jose");

        Cartao cartao = new Cartao();
        cartao.setCliente(cliente);
        cartao.setNumero("123");
        cartao.setAtivo(true);

        CartaoStatusDTO cartaoStatusDTO = new CartaoStatusDTO();
        cartaoStatusDTO.setAtivo(false);

        Mockito.when(cartaoRepository.findByNumero(Mockito.anyString())).thenReturn(cartao);
        Mockito.when(cartaoRepository.save(Mockito.any(Cartao.class))).thenReturn(cartao);

        //Execução
        CartaoSaidaDTO cartaoResultado = cartaoService.alterarStatusAtivo("123", cartaoStatusDTO);

        //Verificação
        Assertions.assertEquals(cartao.getAtivo(), cartaoResultado.getAtivo());
    }

    @Test
    public void testarAlterarStatusAtivoSemEncontrarCartao()
    {
        //Preparação
        CartaoStatusDTO cartaoStatusDTO = new CartaoStatusDTO();
        cartaoStatusDTO.setAtivo(false);

        Mockito.when(cartaoRepository.findByNumero(Mockito.anyString())).thenReturn(null);

        //Execução + Verificação
        Assertions.assertThrows(RuntimeException.class, () -> {cartaoService.alterarStatusAtivo("123", cartaoStatusDTO);});
    }

    @Test
    public void testarBuscarPorNumero()
    {
        //Preparação
        Cliente cliente = new Cliente();
        cliente.setId(1);
        cliente.setName("Jose");

        Cartao cartao = new Cartao();
        cartao.setCliente(cliente);
        cartao.setNumero("123");
        cartao.setAtivo(true);

        CartaoStatusDTO cartaoStatusDTO = new CartaoStatusDTO();
        cartaoStatusDTO.setAtivo(false);

        Mockito.when(cartaoRepository.findByNumero(Mockito.anyString())).thenReturn(cartao);

        //Execução
        CartaoSemStatusDTO cartaoResultado = cartaoService.buscarPorNumero("123");

        //Verificação
        Assertions.assertEquals(cartao.getNumero(), cartaoResultado.getNumero());
    }

    @Test
    public void testarBuscarPorNumeroSemEncontrarCartao()
    {
        //Preparação
        CartaoStatusDTO cartaoStatusDTO = new CartaoStatusDTO();
        cartaoStatusDTO.setAtivo(false);

        Mockito.when(cartaoRepository.findByNumero(Mockito.anyString())).thenReturn(null);

        //Execução + Verificação
        Assertions.assertThrows(RuntimeException.class, () -> {cartaoService.buscarPorNumero("123");});
    }
}
