package br.com.itau.ExercicioCartoes.services;

import br.com.itau.ExercicioCartoes.DTOs.PagamentoEntradaDTO;
import br.com.itau.ExercicioCartoes.DTOs.PagamentoSaidaDTO;
import br.com.itau.ExercicioCartoes.models.Cartao;
import br.com.itau.ExercicioCartoes.models.Cliente;
import br.com.itau.ExercicioCartoes.models.Pagamento;
import br.com.itau.ExercicioCartoes.repositories.PagamentoRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.internal.stubbing.OngoingStubbingImpl;
import org.mockito.stubbing.OngoingStubbing;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@SpringBootTest
public class PagamentoServiceTeste
{
    @MockBean
    private PagamentoRepository pagamentoRepository;

    @Autowired
    private PagamentoService pagamentoService;

    @Test
    public void testarCriar()
    {
        //Preparação
        Cliente cliente = new Cliente();
        cliente.setId(1);
        cliente.setName("Jose");

        Cartao cartao = new Cartao();
        cartao.setId(1);
        cartao.setCliente(cliente);
        cartao.setNumero("123");
        cartao.setAtivo(true);

        Pagamento pagamento = new Pagamento();
        pagamento.setId(1);
        pagamento.setCartao(cartao);
        pagamento.setDescricao("coca cola");
        pagamento.setValor(4.99);

        PagamentoEntradaDTO pagamentoDTO = new PagamentoEntradaDTO();
        pagamentoDTO.setValor(4.99);
        pagamentoDTO.setDescricao("coca cola");
        pagamentoDTO.setCartao_id(1);

        Mockito.when(pagamentoRepository.save(Mockito.any(Pagamento.class))).thenReturn(pagamento);

        //Execução
        PagamentoSaidaDTO pagamentoResultado = pagamentoService.criar(pagamentoDTO);

        //Verificação
        Assertions.assertEquals(pagamento.getValor(), pagamentoResultado.getValor());
    }
}
