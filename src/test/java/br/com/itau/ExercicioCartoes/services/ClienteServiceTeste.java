package br.com.itau.ExercicioCartoes.services;

import br.com.itau.ExercicioCartoes.models.Cliente;
import br.com.itau.ExercicioCartoes.repositories.ClienteRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Optional;

@SpringBootTest
public class ClienteServiceTeste
{
    @MockBean
    private ClienteRepository clienteRepository;

    @Autowired
    private ClienteService clienteService;

    @Test
    public void testarCriar()
    {
        //Preparação
        Cliente cliente = new Cliente();
        cliente.setId(1);
        cliente.setName("Jose");

        Mockito.when(clienteRepository.save(Mockito.any(Cliente.class))).thenReturn(cliente);

        //Execução
        Cliente clienteResultado = clienteService.criar(cliente);

        //Verificação
        Assertions.assertEquals(cliente.getName(), clienteResultado.getName());
    }

    @Test
    public void testarBuscarPorIdEncontrandoCliente()
    {
        //Preparação
        Cliente cliente = new Cliente();
        cliente.setId(1);
        cliente.setName("Jose");

        Optional<Cliente> clienteOptional = Optional.of(cliente);
        Mockito.when(clienteRepository.findById(Mockito.anyInt())).thenReturn(clienteOptional);

        //Execução
        Cliente clienteResultado = clienteService.buscarPorId(1);

        //Verificação
        Assertions.assertEquals(clienteOptional.get(), clienteResultado);
    }

    @Test
    public void testarBuscarPorIdNaoEncontrandoCliente()
    {
        //Preparação
        Cliente cliente = new Cliente();
        cliente.setId(1);
        cliente.setName("Jose");

        Optional<Cliente> clienteOptional = Optional.empty();
        Mockito.when(clienteRepository.findById(Mockito.anyInt())).thenReturn(clienteOptional);

        //Execução + Verificação
        Assertions.assertThrows(RuntimeException.class, () -> {clienteService.buscarPorId(1);});
    }
}
