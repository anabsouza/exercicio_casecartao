package br.com.itau.ExercicioCartoes.controllers;

import br.com.itau.ExercicioCartoes.DTOs.CartaoEntradaDTO;
import br.com.itau.ExercicioCartoes.DTOs.CartaoSaidaDTO;
import br.com.itau.ExercicioCartoes.DTOs.CartaoSemStatusDTO;
import br.com.itau.ExercicioCartoes.DTOs.CartaoStatusDTO;
import br.com.itau.ExercicioCartoes.models.Cartao;
import br.com.itau.ExercicioCartoes.models.Cliente;
import br.com.itau.ExercicioCartoes.services.CartaoService;
import br.com.itau.ExercicioCartoes.services.ClienteService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.stubbing.OngoingStubbing;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Optional;

@WebMvcTest(CartaoController.class)
public class CartaoControllerTeste
{
    @MockBean
    private CartaoService cartaoService;

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void testarCriarCartao() throws Exception
    {
        //Preparação
        CartaoEntradaDTO cartaoEntradaDTO = new CartaoEntradaDTO();
        cartaoEntradaDTO.setClienteId(1);
        cartaoEntradaDTO.setNumero("123");

        CartaoSaidaDTO cartaoSaidaDTO = new CartaoSaidaDTO();
        cartaoSaidaDTO.setAtivo(false);
        cartaoSaidaDTO.setClienteId(1);
        cartaoSaidaDTO.setId(1);
        cartaoSaidaDTO.setNumero("123");

        Mockito.when(cartaoService.criar(Mockito.any(CartaoEntradaDTO.class))).thenReturn(cartaoSaidaDTO);

        ObjectMapper objectMapper = new ObjectMapper();
        String cartaoJson = objectMapper.writeValueAsString(cartaoEntradaDTO);

        //Execução + Verificação
        mockMvc.perform(MockMvcRequestBuilders.post("/cartao")
                .contentType(MediaType.APPLICATION_JSON).content(cartaoJson))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.numero", CoreMatchers.equalTo("123")));
    }

    @Test
    public void testarBuscarPorNumero() throws Exception
    {
        //Preparação
        CartaoEntradaDTO cartaoEntradaDTO = new CartaoEntradaDTO();
        cartaoEntradaDTO.setClienteId(1);
        cartaoEntradaDTO.setNumero("123");

        CartaoSemStatusDTO cartaoSaidaDTO = new CartaoSemStatusDTO();
        cartaoSaidaDTO.setClienteId(1);
        cartaoSaidaDTO.setId(1);
        cartaoSaidaDTO.setNumero("123");

        Mockito.when(cartaoService.buscarPorNumero(Mockito.anyString())).thenReturn(cartaoSaidaDTO);

        //Execução + Verificação
        mockMvc.perform(MockMvcRequestBuilders.get("/cartao/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.equalTo(1)));
    }

    @Test
    public void TestarBuscarPorNumeroSemResposta() throws Exception
    {
        //Preparação
        Mockito.when(cartaoService.buscarPorNumero(Mockito.anyString())).thenThrow(RuntimeException.class);


        mockMvc.perform(MockMvcRequestBuilders.get("/cartao/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void testarAlterarStatusAtivo() throws Exception
    {
        //Preparação
        CartaoStatusDTO cartaoEntradaDTO = new CartaoStatusDTO();
        cartaoEntradaDTO.setAtivo(true);

        CartaoSaidaDTO cartaoSaidaDTO = new CartaoSaidaDTO();
        cartaoSaidaDTO.setAtivo(true);
        cartaoSaidaDTO.setClienteId(1);
        cartaoSaidaDTO.setId(1);
        cartaoSaidaDTO.setNumero("123");

        Mockito.when(cartaoService.alterarStatusAtivo(Mockito.anyString(), Mockito.any(CartaoStatusDTO.class))).thenReturn(cartaoSaidaDTO);

        ObjectMapper objectMapper = new ObjectMapper();
        String cartaoJson = objectMapper.writeValueAsString(cartaoEntradaDTO);

        //Execução + Verificação
        mockMvc.perform(MockMvcRequestBuilders.patch("/cartao/1")
                .contentType(MediaType.APPLICATION_JSON).content(cartaoJson))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.ativo", CoreMatchers.equalTo(true)));
    }

    @Test
    public void TestarAlterarStatusAtivoComErro() throws Exception
    {
        //Preparação
        Mockito.when(cartaoService.alterarStatusAtivo(Mockito.anyString(), Mockito.any(CartaoStatusDTO.class))).thenThrow(RuntimeException.class);

        mockMvc.perform(MockMvcRequestBuilders.patch("/cartao/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }
}
