package br.com.itau.ExercicioCartoes.controllers;

import br.com.itau.ExercicioCartoes.DTOs.PagamentoEntradaDTO;
import br.com.itau.ExercicioCartoes.DTOs.PagamentoSaidaDTO;
import br.com.itau.ExercicioCartoes.models.Cartao;
import br.com.itau.ExercicioCartoes.models.Cliente;
import br.com.itau.ExercicioCartoes.models.Pagamento;
import br.com.itau.ExercicioCartoes.services.ClienteService;
import br.com.itau.ExercicioCartoes.services.PagamentoService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@WebMvcTest(PagamentoController.class)
public class PagamentoControllerTeste
{
    @MockBean
    private PagamentoService pagamentoService;

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void testarCriarPagamento() throws Exception
    {
        //Preparação
        PagamentoEntradaDTO pagamentoDTO = new PagamentoEntradaDTO();
        pagamentoDTO.setValor(4.99);
        pagamentoDTO.setDescricao("coca cola");
        pagamentoDTO.setCartao_id(1);

        PagamentoSaidaDTO pagamentoSaidaDTO = new PagamentoSaidaDTO();
        pagamentoSaidaDTO.setCartao_id(1);
        pagamentoSaidaDTO.setDescricao("coca cola");
        pagamentoSaidaDTO.setId(1);
        pagamentoSaidaDTO.setValor(4.99);

        Mockito.when(pagamentoService.criar(Mockito.any(PagamentoEntradaDTO.class))).thenReturn(pagamentoSaidaDTO);

        ObjectMapper objectMapper = new ObjectMapper();
        String pagamentoJson = objectMapper.writeValueAsString(pagamentoDTO);

        //Execução + Verificação
        mockMvc.perform(MockMvcRequestBuilders.post("/pagamento")
                .contentType(MediaType.APPLICATION_JSON).content(pagamentoJson))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.descricao", CoreMatchers.equalTo("coca cola")));
    }
}
